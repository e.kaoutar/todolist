import React from 'react';
import {Component} from 'react'
import Todos from './components/Todos';
import Header from './components/layout/Header';

class App extends Component{

  state={
    todos:[{
      id:1,
      title:'Take out trash',
      completed:false
    },
    {
      id:2,
      title:'Dinner with wife',
      completed:false
    },
    {
      id:3,
      title:'Meeting with boss',
      completed:false
    }
          ]
  }

  markCompleted = (id) => {
    //alert(id); works
    //we look at state as an object : setState is predefined
    //we wanna change smtg within todos -> state
      //update the completed value
    this.setState({todos: this.state.todos.map(todo => {
      //if the id is equal the one passed in the function
      if(todo.id === id){
        //then we wanna set that todo to false if it's true and to true if it's false : we need to toggle it
        todo.completed = !todo.completed //to do so we'll set it to the opposite all the time

      }
      return todo;
    })});
  }


  //delete a task function --> take id as a parametere
  delTodo = (id) => {
    //alert(id);
      //we need to copy everything that's in the row and filter out the one we want to delete
    this.setState({todos:[...this.state.todos.filter(todo => todo.id !== id)]})
  }

  render(){
    //console.log(this.state.todos)
    return (
      <div className="App">
        <Todos todos={this.state.todos} markCompleted={this.markCompleted} delTodo={this.delTodo} />
      </div>
    );
  }
  
}

export default App;
