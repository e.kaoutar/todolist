import React from 'react';
import {Component} from 'react';
import TodoItems from './TodoItem'
import PropTypes from 'prop-types'

class Todos extends Component{

  render(){
    return this.props.todos.map((todo) => (
      <TodoItems key={todo.id} todo={todo} markCompleted={this.props.markCompleted} delTodo={this.props.delTodo} />
    ))
  }
  
}

//PropTypes -- Good practice
Todos.propTypes = {
  todos: PropTypes.array.isRequired
}

export default Todos;
