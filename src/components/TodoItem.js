import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class TodoItem extends Component {
    
    getStyle = () => {
        return{
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration : this.props.todo.completed? 'line-through' : 'none'
        }
    }

    //we put id and title instead of this.props.todo.id/title
    render() {
        //to not repeat this.props.todo many times --> optimizing
        const {id, title } = this.props.todo;
        return (
            <div style={ this.getStyle() }>
                <p>
                    
                <input type="checkbox" onChange={this.props.markCompleted.bind(this, id)} /> {' '}
                {title}
                <button style={btnStyle} onClick={this.props.delTodo.bind(this, id)} >X</button>
                </p>
                
            </div>
        )
    }
}

//PropTypes -- Good practice
TodoItem.propTypes = {
    todos: PropTypes.object.isRequired
  }


//style variable
const itemStyle = {
    backgroundColor:'#f4f4f4'
}

const btnStyle = {
    background:'#ff0000',
    color: 'white',
    border: 'none',
    padding: '5px 8px',
    borderRadius: '50%',
    cursor: 'pointer',
    float:'right'
}

export default TodoItem
